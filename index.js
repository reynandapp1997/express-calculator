const express = require('express');
const app = express();
const PORT = process.env.PORT || 8080;

const {
    helloWorld,
    hi
} = require('./controller');

const calc = require('./calculator');

app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

app.get('/', helloWorld);

app.post('/hi', hi);

app.post('/calc', calc);

app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
});