const helloWorld = (req, res, next) => {
    res.status(200).send('Hello World')
};

const hi = (req, res, next) => {
    res.status(201).send(`Hi ${req.body.name}`)
}

module.exports = {
    helloWorld,
    hi
}