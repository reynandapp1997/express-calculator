const calc = (req, res, next) => {
    const {
        valOne,
        valTwo,
    } = req.body;
    let result;
    const {
        method
    } = req.query;
    let str;
    switch (method) {
        case 'tambah':
            result = valOne + valTwo;
            str = '+';
            break;
        case 'kurang':
            result = valOne - valTwo;
            str = '-';
            break;
        case 'kali':
            result = valOne * valTwo;
            str = '*';
            break;
        case 'bagi':
            result = valOne / valTwo;
            str = '/';
            break;
        default:
            return res.status(404).json({
                message: 'method not valid'
            });
    }
    res.status(201).json({
        [`${valOne} ${str} ${valTwo}`]: `${result}`
    });
};

module.exports = calc;